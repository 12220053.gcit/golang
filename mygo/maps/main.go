package main

func main(){
	color := map[string] string{
		"red": "love",
		"blue": "positivity",
		"green": "nature",
	}

	printMap(color)

	// color := make(map[string]string)
	// color["red"] = "danger"
	// color ["orange"] = "friut"
	// color["red"] = "love"
	// delete(color, "red")
	// fmt.Println(color)
}

func printMap(c map[string]string){
	for color, def := range c {
		println("definition of" + color + " is" + def)
	}
}

