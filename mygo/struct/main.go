package main

import "fmt"

type contactInfo struct {
	email  string
	mobile int
}
type person struct {
	firstName string
	lastName  string
	contactInfo
}

func main() {
	kuenga := person{
		firstName: "Kuenga",
		lastName:  "Tshering",
		contactInfo: contactInfo{
			email:  "ku@gmail.com",
			mobile: 122233333,
		},
	}

	kuengaPointer := &kuenga
	kuenga.print()
	kuengaPointer.updateName("Deki")
	kuenga.updateName("Pelmo")
	kuenga.print()

}

func (p person) print() {
	fmt.Printf("%v", p)
}

func (pPointer *person) updateName(newfirstName string) {
	(*pPointer).firstName = newfirstName

}


// fmt.printf("%p", kuengaPointer)